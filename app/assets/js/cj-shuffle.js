//////////////////////////////////////////////////

// AVAILABLE API METHOD:
// $("#item-id").nextItem();

//////////////////////////////////////////////////

(function($) {

	var data = {

		target: "_parent",
		imgWidth: 850,
		imgHeight: 350,
		randomize: false,
		delay: 4000,
		margin: 15,
		scaled: 50,
		auto: false

	},

	jQueryMobileUrl = "http://code.jquery.com/mobile/1.0.1/jquery.mobile-1.0.1.min.js",
	touched = "ontouchend" in document,
	mobileLoaded = false,
	script = $("script");

	function scriptEach() {

		mobileLoaded = $(this).attr("src") === jQueryMobileUrl && !mobileLoaded;

	}

	$.fn.cjShuffle = function(settings) {

		return this.each(function() {

			$.extend(data, settings);

			var list = [],
			images = [],
			imgPos = [],
			links = [],
			infos = [],

			target = data.linkTarget,
			imgWidth = data.imageWidth,
			imgHeight = data.imageHeight,
			randomize = data.randomizeItems,
			delay = data.autoPlayDelay,
			margin = data.imageMargin,
			scaled = data.scaleDownBy,
			auto = data.autoPlay,

			shuffle = $(this),
			container = shuffle.children("ul"),
			items = container.children("li"),
			btn = shuffle.find(".shuffle-gallery-button"),
			cloneItem = $("<li />").css("position", "absolute"),

			readyToFire = false,
			isHovering = false,
			isRunning = false,
			switched = true,
			grabOnce = true,
			counter = 0,
			isOn = 0,

			cloneImage,
			fadeHeight,
			fadeWidth,
			preloader,
			fadeVal,
			conList,
			infoOn,
			extraW,
			extraH,
			tCount,
			direct,
			cloneW,
			cloneH,
			cloneX,
			cloneY,
			timer,
			store,
			wasOn,
			fadeX,
			fadeY,
			iLeg,
			btn,
			leg;

			if(!touched) {

				init();

			}
			else if(!mobileLoaded && typeof $.cjShuffleSwipe !== "undefined"){

				$(document).one("mobileinit", init);

				if(typeof $.mobile === "undefined") {

					// attempts to avoid multiple jQuery-Mobile instances
					script.each(scriptEach);

					if(!mobileLoaded) {

						mobileLoaded = true;

						var js = document.createElement("script");
						js.type = "text/javascript";
						js.src = jQueryMobileUrl;

						document.getElementsByTagName("head")[0].appendChild(js);

					}

				}
				else {

					init();

				}

			}
			else {

				init();

			}

			function init(event) {

				if(event) {

					// disable some default jQuery mobile events (we just want the rock solid touch event API)
					$.extend($.mobile, {

						autoInitializePage: false,
						ajaxEnabled: false,
						linkBindingEnabled: false,
						hashListeningEnabled: false,
						pushStateEnabled: false,
						defaultPageTransition: "none",
						defaultDialogTransition: "none",
						loadingMessage: false,
						pageLoadErrorMessage: false

					});

					$.cjShuffleSwipe();

				}

				if(items.length && shuffle.length) {

					leg = items.length;
					iLeg = leg - 1;

					var ar,
					url,
					txt,
					pos,
					preImg,
					titles,
					aligns,
					styles,
					k = leg,
					btnH = 0,
					positions,
					tempList = [],
					tempImages = [],
					tempInfos = [],
					tempLinks = [],
					halfWidth = imgWidth >> 1,
					legMar = imgHeight + (iLeg * margin);

					this.nextItem = clicked;
					preloader = shuffle.find(".shuffle-gallery-preloader");

					if(preloader.length) {

						preImg = preloader.children("img");

						if(preImg.length) {

							preloader.css({

								left: halfWidth - ((parseInt(preImg.attr("width"), 10) * 0.5) | 0),
								top: (legMar >> 1) - ((parseInt(preImg.attr("height"), 10) * 0.5) | 0),
								display: "block"

							});

						}

					}

					if(btn.length) {

						var btnImg = btn.children("img");

						if(btnImg.length) {

							btn.css({

								position: "absolute",
								top: legMar,
								left: halfWidth - (parseInt(btnImg.attr("width"), 10) >> 1)

							});

							btnImg.mouseover(btnOver).mouseout(btnOut);

							if(btnImg.attr("height")) btnH = parseInt(btnImg.attr("height"), 10);

						}

					}

					shuffle.css({width: imgWidth, height: imgHeight + btnH + (margin * iLeg)});
					shuffle.find("ul").css("display", "block");

					fadeWidth = imgWidth;
					fadeY = margin * iLeg;
					fadeX = 0;

					counter = iLeg;

					items.each(function(i) {

						fadeHeight = ((fadeWidth / imgWidth) * imgHeight) | 0;

						tempImages[i] = $("<img />").load(imgLoaded).appendTo($(this));

						tempList[i] = $(this).css("position", "absolute").hide();

						imgPos[i] = {width: fadeWidth, height: fadeHeight, top: fadeY, left: fadeX, zIndex: counter};

						ul = $(this).find("ul").css("display", "none");

						if(ul.length) {

							ul.attr("title") ? tempLinks[i] = ul.attr("title") : tempLinks[i] = 0;

							if(ul.attr("class")) {

								aligns = ul.attr("class").toLowerCase();
								(aligns.search("-") !== -1) ? aligns = aligns.split("-")[1] : aligns = "left";

							}
							else {

								aligns = "left";

							}

						}
						else {

							tempLinks[i] = 0;
							aligns = "left";

						}

						txt = $(this).find("li");

						if(txt.length) {

							titles = [];
							positions = [];
							styles = [];

							txt.each(function(j) {

								titles[j] = 0;
								positions[j] = 0;
								styles[j] = 0;

								if(txt.html() != "") {

									if($(this).attr("class")) {

										pos = $(this).attr("class").toLowerCase();

										if(pos.search("x") !== -1) {

											ar = $(this).attr("class").split("x");

											titles[j] = $(this).html();
											positions[j] = {x: parseInt(ar[0], 10), y: parseInt(ar[1], 10)};
											styles[j] = {color: $(this).css("color"), backgroundColor: $(this).css("background-color")};

										}

									}

								}

							});


							tempInfos[i] = {content: titles, position: positions, style: styles, align: aligns};

						}
						else {

							tempInfos[i] = 0;

						}

						fadeWidth -= scaled;
						fadeX = (imgWidth - fadeWidth) >> 1;

						counter--;
						fadeY -= margin;

					});

					cloneW = fadeWidth;
					cloneH = ((fadeWidth / imgWidth) * imgHeight) | 0;
					cloneX = fadeX;
					cloneY = fadeY;

					fadeWidth = imgWidth + scaled;
					fadeHeight = ((fadeWidth / imgWidth) * imgHeight) | 0;
					fadeX = (imgWidth - fadeWidth) >> 1;
					fadeY = margin * leg;
					counter = 0;

					if(!randomize) {

						list = tempList;
						images = tempImages;
						links = tempLinks;
						infos = tempInfos;

					}

					else {

						var shuf = [], shuf2 = [], shuf3 = [], shuf4 = [], placer, iOn;

						for(k = 0; k < leg; k++) {

							shuf[k] = tempList[k];
							shuf2[k] = tempImages[k];
							shuf3[k] = tempLinks[k];
							shuf4[k] = tempInfos[k];

						}

						while(shuf.length > 0) {

							placer = (Math.random() * shuf.length) | 0;
							iOn = list.length;

							list[iOn] = shuf[placer];
							images[iOn] = shuf2[placer];
							links[iOn] = shuf3[placer];
							infos[iOn] = shuf4[placer];

							shuf.splice(placer, 1);
							shuf2.splice(placer, 1);
							shuf3.splice(placer, 1);
							shuf4.splice(placer, 1);

						}

					}

					k = leg;

					while(k--) {

						if(list[k].attr("title")) {

							pos = imgPos[k];

							list[k].css({

								width: pos.width,
								height: pos.height,
								top: pos.top,
								left: pos.left,
								zIndex: pos.zIndex

							});

							url = list[k].attr("title");
							list[k].data("src", url);

							images[k].attr({width: pos.width, height: pos.height, src: url});
							list[k].removeAttr("title");

						}
						else {

							//alert("item number " + k.toString() + " does not have an image url");

						}

					}

				}
				else {

					//alert("Shuffle Gallery div of image list items could not be found");

				}

				items = settings = null;

			}

			function imgLoaded(event) {

				event.stopPropagation();

				$(event.target).unbind("load", imgLoaded);

				if(counter < iLeg) {

					counter++;

				}
				else {

					if(preloader) preloader.remove();

					list[0].fadeIn(500, fadeTheRest);

					preloader = null;

				}

			}

			function fadeTheRest() {

				counter = 0;
				fadeVal = setInterval(fadeAgain, 100);

			}

			function fadeAgain() {

				if(counter < iLeg) {

					counter++;
					list[counter].fadeIn(500);

				}
				else {

					clearInterval(fadeVal);

					if(auto) shuffle.mouseenter(enterMouse).mouseleave(exitMouse);

					showInfo();
					btn.fadeIn();

					counter = null;

				}

			}

			function clicked(event) {

				if(isRunning) return false;

				if(typeof event === "object") {

					event.stopPropagation();
					isHovering = true;

				}

				if(touched) shuffle.unbind("swipedown", clicked);
				if(btn) btn.unbind("click", clicked);

				if(timer) clearTimeout(timer);

				list[isOn].css("cursor", "auto").unbind("click", gotoURL);

				isRunning = true;
				switched = false;

				cloneImage = $("<img />").attr("width", cloneW).attr("height", cloneH).appendTo(cloneItem).load(cloneLoaded);
				cloneImage.attr("src", list[isOn].data("src"));

			}

			function cloneLoaded(event) {

				event.stopPropagation();

				cloneImage.unbind("load", cloneLoaded);

				if(store != null) {

					while(store.length) {

						store[0].stop().remove();
						store.shift();

					}

					store = null;
					conList = null;

				}

				var i = leg, prop, popped = imgPos.pop(), temp = imgPos, counter = 0;

				imgPos = [popped];
				wasOn = isOn;

				for(i = 1; i < leg; i++) imgPos[i] = temp[i - 1];

				(isOn < iLeg) ? isOn++ : isOn = 0;

				while(i--) {

					if(i !== wasOn) {

						prop = imgPos[i];
						images[i].animate({width: prop.width, height: prop.height}, 400);

						list[i].css("z-index", parseInt(list[i].css("z-index"), 10) + 1).animate({

							width: prop.width,
							height: prop.height,
							top: prop.top,
							left: prop.left

							}, 400);

					}
					else {

						images[i].animate({width: fadeWidth, height: fadeHeight}, 400);

						list[i].css("z-index", parseInt(list[i].css("z-index"), 10) + 1).animate({width: fadeWidth, height: fadeHeight, top: fadeY, left: fadeX, opacity: 0}, 400, showInfo);

					}

				}

				prop = imgPos[wasOn];

				cloneItem.css({

					width: cloneW,
					height: cloneH,
					top: cloneY,
					left: cloneX,
					opacity: 0,
					zIndex: 0

				}).appendTo(container).animate({width: prop.width, height: prop.height, top: prop.top, left: prop.left, opacity: 1}, 400);

				cloneImage.animate({width: prop.width, height: prop.height}, 400, switchClone);

			}

			function switchClone() {

				var prop = imgPos[wasOn];

				cloneImage.remove();
				cloneItem.detach();

				list[wasOn].stop(true, true).css({

					width: prop.width,
					height: prop.height,
					top: prop.top,
					left: prop.left,
					opacity: 1,
					zIndex: parseInt(list[wasOn].css("z-index") - leg, 10)

				});

				images[wasOn].css({width: prop.width, height: prop.height});

				switched = true;
				cloneImage = null;

			}

			function enterMouse(event) {

				isHovering = true;
				if(timer) clearTimeout(timer);

			}

			function exitMouse(event) {

				isHovering = false;
				if(readyToFire) timer = setTimeout(clicked, delay);

			}

			function showInfo() {

				if(switched) {

					clearInterval(fadeVal);

					isRunning = false;

					if(infos[isOn] !== 0) {

						conList = infos[isOn];
						tCount = conList.content.length - 1;
						direct = conList.align;
						infoOn = 0;
						store = [];

						showContent();

					}
					else {
						showDone();
					}

					if(btn) btn.click(clicked);
					if(touched) shuffle.bind("swipedown", clicked);
					if(links[isOn] !== 0) list[isOn].css("cursor", "pointer").click(gotoURL);

				}
				else {

					fadeVal = setInterval(showInfo, 100);

				}

			}

			function gotoURL(event) {

				(target === "_parent") ? window.location = links[isOn] : window.open(links[isOn]);

			}

			function showDone() {

				if(auto && !isRunning && !isHovering) timer = setTimeout(clicked, delay);

				readyToFire = true;

			}

			function showContent() {

				if(isRunning) return;

				var cont = conList.content[infoOn];

				if(cont === 0) {

					if(infoOn < tCount) {
						infoOn++;
						showContent();
					}
					else {
						showDone();
					}

					return;

				}

				var w, h, func, obj, info = $("<div />").html(cont).addClass("shuffle-gallery-info-text").appendTo(list[isOn]);

				if(grabOnce) {

					extraW = parseInt(info.css("padding-left"), 10) + parseInt(info.css("padding-right"), 10);
					extraH = parseInt(info.css("padding-top"), 10) + parseInt(info.css("padding-bottom"), 10);
					grabOnce = false;

				}

				h = info.height() + extraH;
				w = info.width();

				info.css(conList.style[infoOn]).css({

					left: (direct === "left") ? conList.position[infoOn].x : imgWidth - w - extraW - conList.position[infoOn].x,
					top: conList.position[infoOn].y

				}).find("a").each(function() {

					$(this).css(conList.style[infoOn]);

				});

				store[infoOn] = info;

				if(infoOn < tCount) {
					infoOn++;
					func = showContent;
				}
				else {
					func = showDone;
				}

				w += extraW;

				if(direct === "left") {

					info.css("clip", "rect(0px, 0px, " + h + "px, 0px)").animate(

						{zIndex: 100}, {

							duration: 300,
							complete: func,

							step: function(now) {

								info.css({"clip": "rect(0px, " + ((info.width() + extraW) * (now * .01)) + "px, " + h + "px, 0px)"});

							}

						}

					);

				}
				else {

					info.css("clip", "rect(0px, " + w + "px, " + h + "px, " + w + "px)").animate(

						{zIndex: 100}, {

							duration: 300,
							complete: func,

							step: function(now) {

								info.css({"clip": "rect(0px, " + w + "px, " + h + "px, " + (w * (1 - (now * .01))) + "px)"});

							}

						}

					);

				}

			}

		});

	}

	function btnOver(event) {

		$(this).stop(true, true).fadeTo(200, 0.65);

	}

	function btnOut(event) {

		$(this).stop(true, true).fadeTo(200, 1);

	}

})(jQuery);
