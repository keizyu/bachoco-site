/*!
 * Bachoco
 * Bachoco
 * http://keizyu.com
 * @author Keizyu Delgadillo
 * @version 1.0.5
 * Copyright 2018. MIT licensed.
 */

$(document).ready(function(){
    $('.sidenav').sidenav();

    var swiper = new Swiper('.swiper-container', {
     navigation: {
       nextEl: '.swiper-button-next',
       prevEl: '.swiper-button-prev',
     },

    });

    var swiperCarousel = new Swiper('.swiper-carousel', {
      slidesPerView: 'auto',
      centeredSlides: true,
      spaceBetween: 30,
      loop: true,
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },

    });

    $('.parallax').parallax();
    $('.modal').modal();
    $('.tooltip').tooltip({
      margin: 1,
    });
    $('.collapsible').collapsible({
      accordion: false
    });

    // $( ".material-tooltip" ).first().css( "margin-top", "-2.5rem" );
    // $( ".material-tooltip:eq(1)" ).css( "margin-right", "-1rem;" );
    // $( ".material-tooltip:eq(3)" ).css( "background-color", "red" );

    $('#showSearchBar').click(function(event){
      event.preventDefault();
      $('#searchBar').fadeIn();
      $('#search').focus();
    });

    $('#close-searchBar').click(function(){
      $('#searchBar').fadeOut();
    });


  });
